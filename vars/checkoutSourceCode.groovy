def call() {
    // call function example: checkoutSourceCode()

    checkout scm
    stash(name: 'ws', includes: "**", excludes: '**/.git/**')
    stash(name: 'openshift', includes: '**/openshift/**')
}
