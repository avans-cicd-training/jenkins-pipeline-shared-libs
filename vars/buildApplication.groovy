def call() {
    // call function example: buildApplication()

    sh "mvn clean package -Pfatjar -DskipTests"
    stash(name: 'fatjar', includes: '**/target/fatjar.jar')
}
