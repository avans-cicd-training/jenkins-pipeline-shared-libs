def call (Map vars) {
    // call function example: resourceExist(
    //                              project: 'name of project where to deploy',
    //                              appName: 'the application name',
    //                              resource: 'the resource (bc or dc)'
    //                              )
    openshift.withCluster() {
        openshift.withProject(vars.project) {
            def resource = openshift.selector(vars.resource, vars.appName)
            return resource.exists()
        }
    }
}
