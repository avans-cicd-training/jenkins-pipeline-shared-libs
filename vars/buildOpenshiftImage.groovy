def call (Map vars) {
    // call function example: buildOpenshiftImage(
    //                              project: 'name of project where to build',
    //                              appName: 'the application name'
    //                              )
    unstash 'fatjar'
    openshift.withCluster() {
        openshift.withProject(vars.project) {
            openshift.selector("bc", vars.appName).startBuild("--from-file=target/fatjar.jar").logs("-f")
        }
    }
}
