def call() {
    // call function example: mutationTestApplication()

    sh 'mvn org.pitest:pitest-maven:mutationCoverage'
    pitmutation killRatioMustImprove: true, mutationStatsFile: 'target/pit-reports/**/mutations.xml'
}
