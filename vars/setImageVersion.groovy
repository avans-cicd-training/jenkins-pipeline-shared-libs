def call(String majorVersion) {
    // call function example: setImageVersion('2')

    def newVersion = "${majorVersion}.${env.BUILD_NUMBER}-${getCommitId()}"
    echo "New version set to: ${newVersion}"
    return newVersion
}
