def call(Map vars) {
    // call function example: pushImageTagFromToOpenShiftProject(
    //                              fromProject: 'name of project where current image is',
    //                              toProject: 'name of project to push image to',
    //                              appName: 'the application name',
    //                              fromTag: 'the current image tag',
    //                              toTag: 'the new image tag'
    //                              )
    sh(script: "oc tag ${vars.fromProject}/${vars.appName}:${vars.fromTag} ${vars.toProject}/${vars.appName}:${vars.toTag}")
}
