def call() {
    // call function example: getCommitId()
    // function return a string

    commit_id = sh(script: 'git rev-parse --short HEAD', returnStdout: true).trim()
    echo "CommitID: ${commit_id}"
    return commit_id
}
