def call (Map vars) {
    // call function example: robotTestFramework(
    //                              robotFileLocation: 'The location of the robot file from project dir',
    //                              )
    sh "robot -v DOCKER:True ${vars.robotFileLocation}"
    step(
            [
                    $class: 'RobotPublisher',
                    outputPath: '.',
                    passThreshold: 100,
                    unstableThreshold: 100,
                    otherFiles: '*.png'
            ]
    )
}
